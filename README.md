Micra
===
Micra is a very tiny Docker container image that is built from [openSUSE Leap](https://www.opensuse.org/#Leap) packages. It derives it's name from ["Brookesia micra"](https://en.wikipedia.org/wiki/Brookesia_micra), the world's smallest known chameleon species.

Micra doesn't intend to be a seperate Linux distrobuton, as it is made completely from packages provided right from openSUSE Leap.


Building on an openSUSE Leap 42.3 host
---

Run the `./build.bash` script with root priveledges.
```bash
sudo ./build.bash
```


Building inside opensuse:42.3 Docker container
---

```
```
