#!/bin/bash

if [[ $UID != 0 ]]; then
	echo "must run this as root. Try with 'sudo'."
	exit 1
fi

clean() {
	exit 1
}

trap clean SIGTERM SIGKILL SIGQUIT

name=micra 

build_dir=/tmp/
chroot_dir=${build_dir}/${name}
host_repos_dir=/etc/zypp/repos.d
repos_dir=${chroot_dir}/${host_repos_dir}
mkdir -p ${repos_dir}
find $host_repos_dir -type f -name 'repo-*.repo' -exec cp {} ${repos_dir} \;
cd $chroot_dir 
zin() {
	zypper --cache-dir /var/cache/zypper/ -n -R $chroot_dir --releasever 42.3 -n $@
}

zref() {
	zypper --cache-dir /var/cache/zypper/ -n --gpg-auto-import-keys -R $chroot_dir ref --force
}


rpm --root $chroot_dir --rebuilddb
zref && zin busybox-static filesystem glibc

if [[ ${chroot_dir}/usr/bin/busybox-static ]]; then
	cd ${chroot_dir}/usr/bin
	for cmd in `./busybox-static --list` ; do 
		ln -s ./busybox-static $cmd
	done
	cd ../../bin
	for cmd in `../usr/bin/busybox-static --list`; do
		ln -s ../usr/bin/$cmd $cmd
	done
else
	exit 3
fi

zin nodejs8
